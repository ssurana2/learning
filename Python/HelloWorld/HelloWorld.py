print('Hello World!')
print(1 + 2)
print(7 * 6)
print()
print("The End")
print("Phyton's Strings are easy to use")
print('We can even include "quotes" in strings')
print("hello" + " world")
greeting = "Hello"
name = "Sumit"
print(greeting + name)
# if we want a space, we can add that too
print(greeting + ' ' + name)